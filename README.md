
<!-- README.md is generated from README.Rmd. Please edit that file -->

# nichedynamics: Temporal Dynamics and Intra-Specific Flexibility of the Ecological Niche <img src="man/figures/logo.png" align="right" height="138" />

<!-- badges: start -->
<!-- badges: end -->

The aim of the `nichedynamics` package is to offer facilities to explore
the temporal variability of the ecological niche, that is its *temporal
dynamics*. Temporal variations in ecological niches can occur at several
scales, from the various life-cycle periods of an individual, a
population or a species, to circadian rhythm or to long-term variations
across years or decades, and any of these levels can be used as
*dynamics levels* when building and measuring the niche with
`nichedynamics`. `nichedynamics` is also designed to incorporate an
intra-specific flexibility dimension and thus permits measuring various
niche parameters across several intra-specific *flexibility units*.

## Installation

You can install the development version of nichedynamics following:

``` r
# remotes::install_gitlab("/marine_spatial_ecology/nichedynamics", host = "gitlab.univ-lr.fr")
```
